﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotation : MonoBehaviour
{
    [SerializeField]
    private Vector3 direction;
    public float speed;
    // Update is called once per frame
    void Update()
    {
        direction = ClampVector3(direction);
        transform.Rotate(direction * (speed + Time.deltaTime));
    }




    public static Vector3 ClampVector3(Vector3 target)
    {
        float clampedX = Mathf.Clamp(target.x, 0f, 0f);
        float clampedY = Mathf.Clamp(0f, target.y, 0f);
        float clampedZ = Mathf.Clamp(0f, 0f, target.z);
        Vector3 result = new Vector3(clampedX, clampedY, clampedZ);
        return result;
    }
}